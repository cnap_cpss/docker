FROM ubuntu:xenial

RUN apt-get update
RUN apt-get install -qqy apt-utils
RUN apt-get dist-upgrade -y
RUN apt-get install -qqy libqwt5-qt4-dev
RUN apt-get build-dep -qqy gnuradio 
RUN apt-get install -qqy gnuradio
RUN apt-get install -qqy ipython python-crcmod python-bitstring
RUN apt-get install -qqy gcc g++
RUN apt-get install -qqy ssh openssh-server
RUN apt-get install -qqy emacs gedit
RUN apt-get install -qqy git gitg

RUN apt-get install -qqy sudo

RUN apt-get install -qqy net-tools
RUN apt-get install -qqy cmake

RUN apt-get install -qqy htop xterm

#RUN apt-get install -qqy libboost-all-dev libboost-dev

RUN apt-get install -qqy udev

# Create new user and set password
RUN useradd -ms /bin/bash cnap
RUN echo 'cnap:cnap' | chpasswd
RUN echo 'root:cnap' | chpasswd
RUN usermod -aG sudo cnap


# Set up X11 configuration
RUN apt-get install -qqy x11-apps

RUN sudo echo "X11UseLocalhost no" >> /etc/ssh/sshd_config

EXPOSE 22

USER cnap
WORKDIR /home/cnap

USER cnap
WORKDIR /home/cnap
RUN git clone https://code.vt.edu/cnap_cpss/cnap_cpss.git
WORKDIR /home/cnap/cnap_cpss
RUN mkdir build
WORKDIR /home/cnap/cnap_cpss/build
RUN cmake ..
RUN make
USER root
RUN make install
RUN rm -r /home/cnap/cnap_cpss/.git

## Some extra stuff in case you want to use a USRP

USER root
RUN /usr/lib/uhd/utils/uhd_images_downloader.py
RUN cp /lib/udev/rules.d/60-uhd-host.rules /etc/udev/rules.d/.
#RUN udevadm control --reload-rules
#RUN udevadm trigger

RUN ldconfig
ENTRYPOINT service ssh restart && chown -R cnap:cnap /home/cnap && /bin/bash
