
psnum=`sudo docker run -d -p 22 --privileged -v /dev/bus/usb:/dev/bus/usb -v $PWD/working:/home/cnap/working --net="host" -i -t cnap /bin/bash`

ipaddr=`ifconfig docker0 | grep inet | head -n 1 | sed 's/ *inet addr:\([^ ]*\) .*/\1/'`


echo "Container created"
echo "To log in to the container run 'ssh -X cnap@$ipaddr'"
echo "To delete the container run 'sudo docker kill $sps'"

